﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Serilog;

namespace NetCoreApiWithSerilogAndIpFilter.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ValuesController : ControllerBase
    {
        // GET api/values
        [HttpGet]
        public ActionResult<IEnumerable<string>> Get()
        {
            Log.Information("ValuesController : info");
            Log.Warning("ValuesController : warning");
            Log.Error("ValuesController : error");

            return new string[] { "value1", "value2" };
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public ActionResult<string> Get(int id, [FromQuery] string keyword, [FromQuery] int page)
        {
            return "value";
        }

        // POST api/values
        [HttpPost]
        public ActionResult<Val> Post([FromBody] Val value)
        {
            return new Val() { Value = "fch response" };
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] Val value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }

    public class Val
    {
        public string Value { get; set; }
    }
}
