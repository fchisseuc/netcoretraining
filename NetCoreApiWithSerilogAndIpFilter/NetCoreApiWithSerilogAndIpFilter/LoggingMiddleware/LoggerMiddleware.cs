﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Internal;
using Serilog;
using Serilog.Context;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetCoreApiWithSerilogAndIpFilter.LoggingMiddleware
{
    public class LoggerMiddleware
    {
        readonly RequestDelegate _next;

        public LoggerMiddleware(RequestDelegate next)
        {
            if (next == null) throw new ArgumentNullException(nameof(next));
            _next = next;
        }

        public async Task Invoke(HttpContext context)
        {
            if (context == null) throw new ArgumentNullException(nameof(context));

            //no log for swagger requests
            if (context.Request.Path.StartsWithSegments(new PathString("/swagger")))
            {
                await _next(context);
                return;
            }

            var requestBodyContent = await ReadRequestBody(context.Request);

            Log.Information("Request client IP : {@RemoteIpAddress}", context.Connection.RemoteIpAddress.MapToIPv4().ToString());
            Log.Information("Request headers : {@Headers}", context.Request.Headers);
            Log.Information("Request query string : {@QueryString}", context.Request.QueryString.Value);
            Log.Information("Request body : {@Body}", requestBodyContent);

            // The reponse body is also a stream so we need to:
            // - hold a reference to the original response body stream
            // - re-point the response body to a new memory stream
            // - read the response body after the request is handled into our memory stream
            // - copy the response in the memory stream out to the original response stream
            using (var responseBodyMemoryStream = new MemoryStream())
            {
                var originalResponseBodyStream = context.Response.Body;
                context.Response.Body = responseBodyMemoryStream;

                await _next(context);

                string responseBodyContent = null;
                responseBodyContent = await ReadResponseBody(context.Response);

                Log.Information("Response status : {@StatusCode}", context.Response.StatusCode);
                Log.Information("Response headers : {@Headers}", context.Response.Headers);
                Log.Information("Response body : {@Body}", responseBodyContent);

                await responseBodyMemoryStream.CopyToAsync(originalResponseBodyStream);
            }
        }

        private async Task<string> ReadRequestBody(HttpRequest request)
        {
            // Getting the request body is a little tricky because it's a stream
            // So, we need to read the stream and then rewind it back to the beginning
            request.EnableRewind();

            var buffer = new byte[Convert.ToInt32(request.ContentLength)];
            await request.Body.ReadAsync(buffer, 0, buffer.Length);
            var bodyAsText = Encoding.UTF8.GetString(buffer);
            request.Body.Seek(0, SeekOrigin.Begin);

            return bodyAsText;
        }

        private async Task<string> ReadResponseBody(HttpResponse response)
        {
            response.Body.Seek(0, SeekOrigin.Begin);
            var responseBody = await new StreamReader(response.Body).ReadToEndAsync();
            response.Body.Seek(0, SeekOrigin.Begin);

            return responseBody;
        }
    }

}
