﻿using System;
using System.Collections.Generic;
using ToDo.Data.Access;
using ToDo.Data.Model;

namespace ToDo.Data.Business
{
    public class ToDoItemBusiness : IToDoItemBusiness
    {
        IToDoItemRepository _toDoItemRepository;

        public ToDoItemBusiness()
        {
            _toDoItemRepository = new ToDoItemRepository();
        }

        public ToDoItemBusiness(IToDoItemRepository toDoItemRepository)
        {
            _toDoItemRepository = toDoItemRepository;
        }

        public IEnumerable<ToDoItem> GetAll()
        {
            return _toDoItemRepository.GetAll();
        }

        public ToDoItem GetItem(int id)
        {
            return _toDoItemRepository.GetItem(id);
        }
    }
}
