﻿using System.Collections.Generic;
using ToDo.Data.Model;

namespace ToDo.Data.Business
{
    public interface IToDoItemBusiness
    {
        IEnumerable<ToDoItem> GetAll();
        ToDoItem GetItem(int id);
    }
}