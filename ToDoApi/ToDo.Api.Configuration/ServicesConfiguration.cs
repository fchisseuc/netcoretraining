﻿using Microsoft.Extensions.DependencyInjection;
using System;
using ToDo.Data.Access;
using Microsoft.EntityFrameworkCore;
using ToDo.Data.Business;
using AutoMapper.Configuration;
using AutoMapper;

namespace ToDo.Api.Configuration
{
    public class ServicesConfiguration
    {
        public static void RegisterServices(IServiceCollection services)
        {
            RegisterScopedServices(services);
            RegisterSingletonServices(services);
            RegisterTransientServices(services);
            RegisterAutoMapperService(services);
        }

        /// <summary>
        /// Scoped: IoC container will create an instance of the specified service type once per request and will be shared in a single request.
        /// </summary>
        private static void RegisterScopedServices(IServiceCollection services)
        {
            if(services == null)
            {
                return;
            }

            services = services.AddScoped<IToDoItemBusiness, ToDoItemBusiness>();
        }

        /// <summary>
        /// Singleton: IoC container will create and share a single instance of a service throughout the application's lifetime.
        /// </summary>
        private static void RegisterSingletonServices(IServiceCollection services)
        {
        }

        /// <summary>
        /// Transient: The IoC container will create a new instance of the specified service type every time you ask for it
        /// </summary>
        private static void RegisterTransientServices(IServiceCollection services)
        {
        }

        private static void RegisterAutoMapperService(IServiceCollection services)
        {
            if (services == null)
            {
                return;
            }

            services.AddAutoMapper();
        }
    }
}
