﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ToDo.Api.Configuration.Profile
{
    public class MappingProfile: AutoMapper.Profile
    {
        public MappingProfile()
        {
            CreateMap<Data.Model.ToDoItem, Api.Model.ToDoItem>().ReverseMap();
        }
    }
}
