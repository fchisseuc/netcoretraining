﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ToDo.Data.Access;
using ToDo.Data.Business;
using AutoMapper;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace ToDoApi.Controllers
{
    [Route("api/[controller]")]
    public class ToDoController : Controller
    {
        private readonly IToDoItemBusiness _toDoItemBusiness;
        private readonly IMapper _mapper;

        public ToDoController(IMapper mapper, IToDoItemBusiness toDoItemBusiness)
        {
            _mapper = mapper;
            _toDoItemBusiness = toDoItemBusiness;
        }

        //// GET: api/<controller>
        [HttpGet]
        public ActionResult<IEnumerable<ToDo.Api.Model.ToDoItem>> GetAll()
        {
            var items = _toDoItemBusiness.GetAll();
            if(items == null)
            {
                return NotFound();
            }

            var result = _mapper.Map<IEnumerable<ToDo.Data.Model.ToDoItem>, IEnumerable<ToDo.Api.Model.ToDoItem>>(items);
            return Ok(result);
        }

        // GET api/<controller>/5
        [HttpGet("{id}")]
        public ActionResult<ToDo.Api.Model.ToDoItem> Get(int id)
        {
            var item = _toDoItemBusiness.GetItem(id);
            if (item == null)
            {
                return NotFound();
            }

            var result = _mapper.Map<ToDo.Data.Model.ToDoItem, ToDo.Api.Model.ToDoItem>(item);
            return Ok(result);
        }

        //// POST api/<controller>
        //[HttpPost]
        //public void Post([FromBody]string value)
        //{
        //}

        //// PUT api/<controller>/5
        //[HttpPut("{id}")]
        //public void Put(int id, [FromBody]string value)
        //{
        //}

        //// DELETE api/<controller>/5
        //[HttpDelete("{id}")]
        //public void Delete(int id)
        //{
        //}
    }
}
