﻿using System;
using Microsoft.EntityFrameworkCore;
using ToDo.Data.Model;

namespace ToDo.Data.Access
{
    public class ToDoContext:DbContext
    {
        public ToDoContext()
        {
        }

        public ToDoContext(DbContextOptions options):base(options)
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);
            optionsBuilder.UseInMemoryDatabase("TodoList");
        }

        public DbSet<ToDoItem> ToDoItems { get; set; }
    }
}
