﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ToDo.Data.Model;

namespace ToDo.Data.Access
{
    public class ToDoItemRepository : IToDoItemRepository
    {
        private readonly ToDoContext _toDoContext;

        public ToDoItemRepository(): this(new ToDoContext())
        {
        }

        public ToDoItemRepository(ToDoContext toDoContext)
        {
            _toDoContext = toDoContext;
            InitData();
        }

        public IEnumerable<ToDoItem> GetAll()
        {
            return _toDoContext.ToDoItems.ToList();
        }

        public ToDoItem GetItem(int id)
        {
            return _toDoContext.ToDoItems.Find(id);
        }

        private void InitData()
        {
            if (_toDoContext.ToDoItems.Count() == 0)
            {
                _toDoContext.Add(new ToDoItem() { Id = 1, Name = "todo 1" });
                _toDoContext.Add(new ToDoItem() { Id = 2, Name = "todo 2" });
                _toDoContext.SaveChanges();
            }
        }
    }
}
