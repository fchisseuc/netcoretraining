﻿using System.Collections.Generic;
using ToDo.Data.Model;

namespace ToDo.Data.Access
{
    public interface IToDoItemRepository
    {
        IEnumerable<ToDoItem> GetAll();
        ToDoItem GetItem(int id);
    }
}