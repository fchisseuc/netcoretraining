﻿using Microsoft.Extensions.DependencyInjection;
using System;
using ToDo.Data.Access;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace ToDo.Api.Conf
{
    public class Class1
    {
        public void AddDbContext(IServiceCollection services)
        {
            if (services == null)
            {
                return;
            }

            services.AddDbContext<ToDoContext>(opt => opt.UseInMemoryDatabase("TodoList"));
        }
    }
}
