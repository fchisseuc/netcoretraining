﻿using HealthChecks.UI.Client;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics.HealthChecks;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using R2d2ThirdPartiesHealthCheckApi.Configuration;
using System.Linq;

namespace R2d2ThirdPartiesHealthCheckApi.Middleware
{
    public static class HealthCheckMiddleware
    {
        public static void AddHealthCheckService(this IServiceCollection services, IConfiguration configuration)
        {
            //add health check service
            var healthCheckBuilder = services.AddHealthChecks();

            //get third parties uris to check health
            var thirdPartiesUrisSettings = configuration.BindThirdPartiesUriSettings();

            //add health check on each uri
            if (thirdPartiesUrisSettings != null && thirdPartiesUrisSettings.Count > 0)
            {
                foreach (var healthCheckSetting in thirdPartiesUrisSettings)
                    healthCheckBuilder.AddUrlGroup(new System.Uri(healthCheckSetting.Uri), healthCheckSetting.Name);
            }
        }

        public static void UseHealthCheckMiddleware(this IApplicationBuilder app)
        {
            //HealthCheck middleware
            app.UseHealthChecks("/healthcheck", new HealthCheckOptions()
            {
                Predicate = healthCheckRegistration => true,
                ResponseWriter = UIResponseWriter.WriteHealthCheckUIResponse
            });
        }
    }
}
