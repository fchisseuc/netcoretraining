﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace R2d2ThirdPartiesHealthCheckApi.Configuration
{
    public static class ConfigurationExtensions
    {
        internal const string THIRDPARTIES_HEALTHCHECKS_URIS_SETTING_KEY = "ThirdPartiesHealthChecksUris:HealthChecks";

        public static ICollection<HealthCheckSetting> BindThirdPartiesUriSettings(this IConfiguration configuration)
        {
            ICollection<HealthCheckSetting> settings = new List<HealthCheckSetting>();

            configuration
                .GetSection(THIRDPARTIES_HEALTHCHECKS_URIS_SETTING_KEY)
                .Bind(settings);

            return settings;
        }
    }
}
