﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace R2d2ThirdPartiesHealthCheckApi.Configuration
{
    public class HealthCheckSetting
    {
        public string Name { get; set; }
        public string Uri { get; set; }
    }
}
