﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace HealthCheckUI.Config
{
    public static class HealthCheckUiConfig
    {
        public static void AddHealthCheckUIService(this IServiceCollection services)
        {
            services.AddHealthChecksUI();
        }

        public static void UseHealthCheckUIMiddleware(this IApplicationBuilder app)
        {
            //HealthCheck UI middleware
            app.UseHealthChecksUI(h =>
            {
                h.UIPath = "/healthcheck-ui";

                //customize CSS
                h.AddCustomStylesheet("./Style/CustomHealthCheckUI.css");
            });
        }
    }
}
