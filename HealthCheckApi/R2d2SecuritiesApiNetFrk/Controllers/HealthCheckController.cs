﻿using R2d2SecuritiesApiNetFrk.Models.HealthCheck.Database;
using R2d2SecuritiesApiNetFrk.Models.HealthCheck.Uri;
using System.Threading.Tasks;
using System.Web.Http;
using System.Configuration;
using R2d2SecuritiesApiNetFrk.Models.HealthCheck.Registration;

namespace R2d2SecuritiesApiNetFrk.Controllers
{
    [RoutePrefix("healthCheck")]
    public class HealthCheckController : ApiController
    {
        IHealthChecksBuilder _healthChecksBuilder;

        public HealthCheckController(IHealthChecksBuilder healthChecksBuilder)
        {
            _healthChecksBuilder = healthChecksBuilder;
        }

        [Route("")]
        [HttpGet]
        public async Task<IHttpActionResult> GetHealthReport()
        {
            var healthReport = await _healthChecksBuilder
                .AddGLEntriesApiHealthCheck(uri: ConfigurationManager.AppSettings["HealthCheck:GlEntriesApiUri"])
                //.AddUri
                //(
                //    name: "GL ??", 
                //    options: new UriHealthCheckOptions(ConfigurationManager.AppSettings["HealthCheck:GlEntriesApiUri"])
                //            .UsePost()
                //)
                .AddSqlServer(connectionString: ConfigurationManager.AppSettings["HealthCheck:DbConnection"], 
                    databaseName: ConfigurationManager.AppSettings["HealthCheck:DbName"])
                .CheckHealthAsync();

            return Ok(healthReport);
        }
    }
}
