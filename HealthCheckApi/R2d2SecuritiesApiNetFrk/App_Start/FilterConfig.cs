﻿using System.Web;
using System.Web.Mvc;

namespace R2d2SecuritiesApiNetFrk
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
