﻿using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Http;

namespace R2d2SecuritiesApiNetFrk.App_Start
{
    public class JsonFormatterConfig
    {
        public static void Register(HttpConfiguration config)
        {
            //config.Formatters.JsonFormatter.SerializerSettings = new Newtonsoft.Json.JsonSerializerSettings();
            //get current json formatter
            var jsonFormatter = config.Formatters.JsonFormatter;

            //ensure that the JSON format is invoked when a consumer requests text HTML
            jsonFormatter.SupportedMediaTypes.Add(new MediaTypeHeaderValue("text/html"));

            //use Newtonsoft Json settings
            jsonFormatter.SerializerSettings = new Newtonsoft.Json.JsonSerializerSettings();

            //use camelcase for query params and fields from in/out bodies
            jsonFormatter.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
        }
    }
}