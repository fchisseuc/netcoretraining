using R2d2SecuritiesApiNetFrk.App_Start;
using R2d2SecuritiesApiNetFrk.Config;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace R2d2SecuritiesApiNetFrk
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            GlobalConfiguration.Configure(JsonFormatterConfig.Register);
            GlobalConfiguration.Configure(IocConfig.Register);
        }
    }
}
