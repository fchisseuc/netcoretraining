﻿using System;
using System.Data.Common;
using System.Threading.Tasks;

namespace R2d2SecuritiesApiNetFrk.Models.HealthCheck.Database
{
    public abstract class DatabaseConnectionHealthCheck : IHealthCheck
    {
        private const string DESCRIPTION_WITHOUT_DATABASE = "Database connection";
        private const string DESCRIPTION_WITH_DATABASE = "Database connection to '{0}'";

        protected DatabaseConnectionHealthCheck(string connectionString, string databaseName, string healthQuery)
        {
            ConnectionString = connectionString ?? throw new ArgumentNullException(nameof(connectionString));
            HealthQuery = healthQuery ?? throw new ArgumentNullException(nameof(healthQuery));
            DatabaseName = databaseName;
        }

        protected abstract DbConnection CreateConnection(string connectionString);

        public async Task<HealthCheckResult> CheckHealthAsync(HealthCheckContext context)
        {
            using (var connection = CreateConnection(ConnectionString))
            {
                try
                {
                    await connection.OpenAsync();
                    using (var command = connection.CreateCommand())
                    {
                        command.CommandText = HealthQuery;
                        await command.ExecuteScalarAsync();
                    }

                    return HealthCheckResult.Healthy(HealthCheckResultDescription);
                }
                catch (DbException ex)
                {
                    return new HealthCheckResult(status: context.Registration.FailureStatus, HealthCheckResultDescription, exception: ex);
                }
            }
        }

        protected string ConnectionString { get; }
        protected string HealthQuery { get; }
        protected string DatabaseName { get; }
        protected string HealthCheckResultDescription
        {
            get {
                return string.IsNullOrWhiteSpace(DatabaseName) ? 
                    DESCRIPTION_WITHOUT_DATABASE : 
                    string.Format(DESCRIPTION_WITH_DATABASE, DatabaseName);
            }
        }
    }
}