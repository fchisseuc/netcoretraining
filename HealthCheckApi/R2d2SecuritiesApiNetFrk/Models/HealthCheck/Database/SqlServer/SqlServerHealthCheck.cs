﻿using System.Data.SqlClient;
using System.Data.Common;

namespace R2d2SecuritiesApiNetFrk.Models.HealthCheck.Database
{
    public class SqlServerHealthCheck : DatabaseConnectionHealthCheck
    {
        private const string DEFAULT_HEALTH_QUERY = "SELECT 1;";

        public SqlServerHealthCheck(string connectionString, string databaseName, string healthQuery)
            : base(connectionString, databaseName, healthQuery ?? DEFAULT_HEALTH_QUERY)
        {
        }

        protected override DbConnection CreateConnection(string connectionString)
        {
            return new SqlConnection(connectionString);
        }
    }
}