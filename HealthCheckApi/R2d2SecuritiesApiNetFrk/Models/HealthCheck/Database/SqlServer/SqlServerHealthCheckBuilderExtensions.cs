﻿using R2d2SecuritiesApiNetFrk.Models.Health;
using R2d2SecuritiesApiNetFrk.Models.HealthCheck.Registration;

namespace R2d2SecuritiesApiNetFrk.Models.HealthCheck.Database
{
    public static class SqlServerHealthCheckBuilderExtensions
    {
        private const string NAME = "Sqlserver (.net frk)";

        /// <summary>
        /// Add a health check for SqlServer services.
        /// </summary>
        public static IHealthChecksBuilder AddSqlServer(this IHealthChecksBuilder builder,
            string connectionString,
            string databaseName = null,
            string name = null,
            string healthQuery = null,
            HealthStatus? failureStatus = HealthStatus.Unhealthy)
        {
            return builder.Add(new HealthCheckRegistration(
                name ?? NAME,
                new SqlServerHealthCheck(connectionString: connectionString, 
                    databaseName: databaseName, 
                    healthQuery: healthQuery),
                failureStatus));
        }
    }
}