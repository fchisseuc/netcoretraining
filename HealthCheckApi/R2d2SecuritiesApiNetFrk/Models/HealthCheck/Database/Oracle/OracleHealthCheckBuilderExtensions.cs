﻿using R2d2SecuritiesApiNetFrk.Models.Health;
using R2d2SecuritiesApiNetFrk.Models.HealthCheck.Registration;

namespace R2d2SecuritiesApiNetFrk.Models.HealthCheck.Database
{
    public static class OracleHealthCheckBuilderExtensions
    {
        const string NAME = "Oracle (.net frk)";

        /// <summary>
        /// Add a health check for Oracle services.
        /// </summary>
        public static IHealthChecksBuilder AddOracle(this IHealthChecksBuilder builder, 
            string connectionString,
            string databaseName = null,
            string name = null,
            string healthQuery = null,
            HealthStatus? failureStatus = HealthStatus.Unhealthy)
        {
            return builder.Add(new HealthCheckRegistration(
                name ?? NAME,
                new OracleHealthCheck(connectionString: connectionString,
                    databaseName: databaseName,
                    healthQuery: healthQuery),
                failureStatus));
        }
    }
}