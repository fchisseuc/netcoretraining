﻿
using Oracle.ManagedDataAccess.Client;
using System.Data.Common;

namespace R2d2SecuritiesApiNetFrk.Models.HealthCheck.Database
{
    public class OracleHealthCheck : DatabaseConnectionHealthCheck
    {
        private const string DEFAULT_HEALTH_QUERY = "SELECT * FROM V$VERSION";

        public OracleHealthCheck(string connectionString, string databaseName, string healthQuery)
            : base(connectionString, databaseName, healthQuery ?? DEFAULT_HEALTH_QUERY)
        {
        }

        protected override DbConnection CreateConnection(string connectionString)
        {
            return new OracleConnection(connectionString);
        }
    }
}