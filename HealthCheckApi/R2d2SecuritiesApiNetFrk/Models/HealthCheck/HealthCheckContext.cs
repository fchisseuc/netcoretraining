﻿using R2d2SecuritiesApiNetFrk.Models.HealthCheck.Registration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace R2d2SecuritiesApiNetFrk.Models.HealthCheck
{
    public class HealthCheckContext
    {
        /// <summary>
        /// Gets or sets the HealthCheckRegistration of the currently executing IHealthCheck
        /// </summary>
        public HealthCheckRegistration Registration { get; set; }
    }
}