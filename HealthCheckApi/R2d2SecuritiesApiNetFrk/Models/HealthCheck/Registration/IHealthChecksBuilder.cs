﻿using R2d2SecuritiesApiNetFrk.Models.Health;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace R2d2SecuritiesApiNetFrk.Models.HealthCheck.Registration
{
    /// <summary>
    /// A builder used to register health checks.
    /// </summary>
    public interface IHealthChecksBuilder
    {
        /// <summary>
        /// Adds a Registration for a health check.
        /// </summary>
        IHealthChecksBuilder Add(HealthCheckRegistration registration);

        /// <summary>
        /// Collection of Registrations registered
        /// </summary>
        ICollection<HealthCheckRegistration> Registrations { get; }

        System.Threading.Tasks.Task<HealthReport> CheckHealthAsync();
    }
}