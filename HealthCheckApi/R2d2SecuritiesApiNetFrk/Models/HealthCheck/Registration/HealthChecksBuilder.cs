﻿using R2d2SecuritiesApiNetFrk.Models.Health;
using R2d2SecuritiesApiNetFrk.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace R2d2SecuritiesApiNetFrk.Models.HealthCheck.Registration
{
    public class HealthChecksBuilder : IHealthChecksBuilder
    {
        public HealthChecksBuilder(IHealthCheckService healthCheckService)
        {
            HealthCheckService = healthCheckService ?? throw new ArgumentNullException(nameof(healthCheckService));
            Registrations = new List<HealthCheckRegistration>();
        }

        public IHealthChecksBuilder Add(HealthCheckRegistration registration)
        {
            if (registration == null)
            {
                throw new ArgumentNullException(nameof(registration));
            }

            Registrations.Add(registration);
            return this;
        }

        public async System.Threading.Tasks.Task<HealthReport> CheckHealthAsync()
        {
            return await HealthCheckService.CheckHealthAsync(Registrations);
        }

        public IHealthCheckService HealthCheckService { get; }

        public ICollection<HealthCheckRegistration> Registrations { get; }
    }
}