﻿using R2d2SecuritiesApiNetFrk.Models.Health;
using R2d2SecuritiesApiNetFrk.Models.HealthCheck;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace R2d2SecuritiesApiNetFrk.Models.HealthCheck.Registration
{
    public class HealthCheckRegistration
    {
        private string _name;

        /// <summary>
        /// Creates a new registration for an existing IHealthCheck instance.
        /// </summary>
        public HealthCheckRegistration(string name, IHealthCheck instance, HealthStatus? failureStatus)
        {
            if (name == null)
            {
                throw new ArgumentNullException(nameof(name));
            }

            if (instance == null)
            {
                throw new ArgumentNullException(nameof(instance));
            }

            Name = name;
            HealthCheckInstance = instance;
            FailureStatus = failureStatus ?? HealthStatus.Unhealthy;
        }

        /// <summary>
        /// Gets or sets the IHealthCheck that will be used to check the status of a component in the application
        /// </summary>
        public IHealthCheck HealthCheckInstance { get; set; }

        /// <summary>
        /// Gets or sets the HealthStatus that should be reported upon failure of the health check.
        /// </summary>
        public HealthStatus FailureStatus { get; set; }

        /// <summary>
        /// Gets or sets the health check name.
        /// </summary>
        public string Name
        {
            get => _name;
            set
            {
                if (value == null)
                {
                    throw new ArgumentNullException(nameof(value));
                }

                _name = value;
            }
        }
    }
}