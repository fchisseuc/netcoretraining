﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
namespace R2d2SecuritiesApiNetFrk.Models.HealthCheck.Uri
{
    public class UriHealthCheck : IHealthCheck
    {
        private const string DESCRIPTION_FORMAT = "Request to '{0}' (current status code : {1})";

        public UriHealthCheck(UriHealthCheckOptions options)
        {
            Options = options ?? throw new ArgumentNullException(nameof(options));
        }

        public async Task<HealthCheckResult> CheckHealthAsync(HealthCheckContext context)
        {
            try
            {
                using (var httpClient = new HttpClient())
                {
                    var requestMessage = new HttpRequestMessage(Options.HttpMethod, Options.RequestUri);

                    var response = await httpClient.SendAsync(requestMessage);
                    var responseStatusCode = (int)response.StatusCode;

                    if (responseStatusCode >= Options.ExpectedHttpCodes.Min &&
                        responseStatusCode <= Options.ExpectedHttpCodes.Max)
                        return HealthCheckResult.Healthy(description: string.Format(DESCRIPTION_FORMAT, Options.RequestUri, responseStatusCode));
                    else
                        return HealthCheckResult.Unhealthy(description: string.Format(DESCRIPTION_FORMAT, Options.RequestUri, responseStatusCode));
                }
            }
            catch (Exception ex)
            {
                return new HealthCheckResult(context.Registration.FailureStatus, exception: ex);
            }
        }

        protected UriHealthCheckOptions Options { get; }
    }
}