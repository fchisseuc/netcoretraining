﻿using R2d2SecuritiesApiNetFrk.Models.Health;
using R2d2SecuritiesApiNetFrk.Models.HealthCheck.Registration;
using System;

namespace R2d2SecuritiesApiNetFrk.Models.HealthCheck.Uri
{
    public static class UriHealthCheckBuilderExtensions
    {
        // <summary>
        /// Add a health check for Uri request
        /// </summary>
        public static IHealthChecksBuilder AddUri(this IHealthChecksBuilder builder,
            string name,
            UriHealthCheckOptions options,
            HealthStatus? failureStatus = HealthStatus.Unhealthy)
        {
            return builder.Add(new HealthCheckRegistration(
                name ?? throw new ArgumentNullException(nameof(name)),
                new UriHealthCheck(options),
                failureStatus));
        }
    }
}