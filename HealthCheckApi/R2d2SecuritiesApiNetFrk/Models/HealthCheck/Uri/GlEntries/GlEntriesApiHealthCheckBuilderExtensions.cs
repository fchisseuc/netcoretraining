﻿using R2d2SecuritiesApiNetFrk.Models.Health;
using R2d2SecuritiesApiNetFrk.Models.HealthCheck.Registration;
using System.Net;

namespace R2d2SecuritiesApiNetFrk.Models.HealthCheck.Uri
{
    public static class GlEntriesApiHealthCheckBuilderExtensions
    {
        private const string NAME = "GL entries health check (.net frk)";

        /// <summary>
        /// Add a health check for GL entries API
        /// </summary>
        public static IHealthChecksBuilder AddGLEntriesApiHealthCheck(this IHealthChecksBuilder builder,
            string uri,
            HealthStatus? failureStatus = HealthStatus.Unhealthy)
        {
            return builder.Add(new HealthCheckRegistration(
                NAME,
                new UriHealthCheck(GetGlEntriesApiHealthCheckOptions(uri)),
                failureStatus));
        }

        public static UriHealthCheckOptions GetGlEntriesApiHealthCheckOptions(string uri)
        {
            return new UriHealthCheckOptions(uri)
                .UseGet()
                .ExpectHttpCode(HttpStatusCode.OK);
        }
    }
}