﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;

namespace R2d2SecuritiesApiNetFrk.Models.HealthCheck.Uri
{
    public class UriHealthCheckOptions
    {
        private static readonly HttpMethod DefaultHttpMethod = HttpMethod.Get;

        public UriHealthCheckOptions(System.Uri uri)
        {
            if (uri == null) throw new ArgumentNullException(nameof(uri));

            RequestUri = uri;
            ExpectedHttpCodes = (Min: 200, Max: 299); // DEFAULT = HTTP Successful status codes
            HttpMethod = DefaultHttpMethod;
        }

        public UriHealthCheckOptions(string uri):this(new System.Uri(uri))
        {
        }

        internal System.Uri RequestUri { get; private set; }
        internal HttpMethod HttpMethod { get; private set; }
        internal (int Min, int Max) ExpectedHttpCodes { get; private set; }

        //public UriHealthCheckOptions AddRequestUri(string uri)
        //{
        //    RequestUri = new System.Uri(uri);
        //    return this;
        //}

        //public UriHealthCheckOptions AddRequestUri(System.Uri uri)
        //{
        //    RequestUri = uri;
        //    return this;
        //}

        public UriHealthCheckOptions UseGet()
        {
            HttpMethod = HttpMethod.Get;
            return this;
        }
        public UriHealthCheckOptions UsePost()
        {
            HttpMethod = HttpMethod.Post;
            return this;
        }

        public UriHealthCheckOptions UseHttpMethod(HttpMethod methodToUse)
        {
            HttpMethod = methodToUse;
            return this;
        }

        public UriHealthCheckOptions ExpectHttpCode(HttpStatusCode codeToExpect)
        {
            ExpectedHttpCodes = ((int)codeToExpect, (int)codeToExpect);
            return this;
        }
    }
}