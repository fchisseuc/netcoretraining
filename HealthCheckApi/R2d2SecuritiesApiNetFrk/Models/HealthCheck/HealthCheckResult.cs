﻿using R2d2SecuritiesApiNetFrk.Models.Health;
using System;
using System.Runtime.Serialization;

namespace R2d2SecuritiesApiNetFrk.Models.HealthCheck
{
    /// <summary>
    /// Represents the result of a health check.
    /// </summary>
    public class HealthCheckResult
    {
        /// <summary>
        /// Creates a new HealthCheckResult with the specified values
        /// </summary>
        public HealthCheckResult(HealthStatus status, string description = null, Exception exception = null)
        {
            Status = status;
            Description = description;
            Exception = exception;
        }

        public HealthStatus Status { get; set; }

        public string Description { get; set; }

        public Exception Exception { get; set; }

        /// <summary>
        /// Creates a HealthCheckResult representing a healthy component.
        /// </summary>
        public static HealthCheckResult Healthy(string description = null)
        {
            return new HealthCheckResult(status: HealthStatus.Healthy, description, exception: null);
        }

        /// <summary>
        /// Creates a HealthCheckResult representing a degraded component.
        /// </summary>
        public static HealthCheckResult Degraded(string description = null, Exception exception = null)
        {
            return new HealthCheckResult(status: HealthStatus.Degraded, description, exception: null);
        }

        /// <summary>
        /// Creates a HealthCheckResult representing an unhealthy component.
        /// </summary>
        public static HealthCheckResult Unhealthy(string description = null, Exception exception = null)
        {
            return new HealthCheckResult(status: HealthStatus.Unhealthy, description, exception);
        }
    }
}
