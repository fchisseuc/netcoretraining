﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace R2d2SecuritiesApiNetFrk.Models.HealthCheck
{
    /// <summary>
    /// Represents a health check, which can be used to check the status of a component in the application, such as a backend service, database ...
    /// </summary>
    public interface IHealthCheck
    {
        /// <summary>
        /// Runs the health check, returning the status of the component being checked.
        /// </summary>
        Task<HealthCheckResult> CheckHealthAsync(HealthCheckContext context);
    }
}