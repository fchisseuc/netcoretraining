﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace R2d2SecuritiesApiNetFrk.Models.Health
{
    /// <summary>
    /// Represents the result of executing a group of IHealthCheck instances.
    /// </summary>
    public class HealthReport
    {
        public HealthReport(Dictionary<string, HealthReportEntry> entries, TimeSpan totalDuration)
        {
            if (entries == null)
                throw new ArgumentNullException(nameof(entries));

            if (entries.Count == 0)
                throw new ArgumentException("Entries list should not be empty", nameof(entries));

            Entries = entries;
            Status = AggregateStatus(entries.Values);
            TotalDuration = totalDuration;
        }

        public Dictionary<string, HealthReportEntry> Entries { get; }

        public HealthStatus Status { get; }

        public TimeSpan TotalDuration { get; }

        private HealthStatus AggregateStatus(IEnumerable<HealthReportEntry> entries)
        {
            if (entries.Select(e => e.Status).Contains(HealthStatus.Unhealthy))
                return HealthStatus.Unhealthy;

            return entries.Select(e => e.Status).Min();
        }
    }
}
