﻿namespace R2d2SecuritiesApiNetFrk.Models.Health
{
    /// <summary>
    /// Represents the reported status of a health check result.
    /// </summary>
    public enum HealthStatus
    {
        Unhealthy = 0,
        Degraded = 1,
        Healthy = 2,
    }
}
