﻿using System;

namespace R2d2SecuritiesApiNetFrk.Models.Health
{
    /// <summary>
    /// Represents an entry in a HealthReport. Corresponds to the result of a single IHealthCheck
    /// </summary>
    public class HealthReportEntry
    {
        public HealthStatus Status { get; set; }

        public TimeSpan Duration { get; set; }

        public string Description { get; set; }

        public Exception Exception { get; set; }
    }
}
