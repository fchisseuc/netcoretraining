﻿using R2d2SecuritiesApiNetFrk.Models.Health;
using R2d2SecuritiesApiNetFrk.Models.HealthCheck.Registration;
using System.Collections.Generic;

namespace R2d2SecuritiesApiNetFrk.Services
{
    public interface IHealthCheckService
    {
        System.Threading.Tasks.Task<HealthReport> CheckHealthAsync(IEnumerable<HealthCheckRegistration> registrations);
    }
}