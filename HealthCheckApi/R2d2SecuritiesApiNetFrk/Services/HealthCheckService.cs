﻿using R2d2SecuritiesApiNetFrk.Models.Health;
using R2d2SecuritiesApiNetFrk.Models.HealthCheck;
using R2d2SecuritiesApiNetFrk.Models.HealthCheck.Registration;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace R2d2SecuritiesApiNetFrk.Services
{
    /// <summary>
    /// A service which can be used to check the status of IHealthCheck instances 
    /// registered in the application.
    public class HealthCheckService : IHealthCheckService
    {
        /// <summary>
        /// Runs the provided health checks and returns the aggregated status
        /// </summary>
        public async Task<HealthReport> CheckHealthAsync(IEnumerable<HealthCheckRegistration> registrations)
        {
            ValidateRegistrations(registrations);

            var context = new HealthCheckContext();
            var entries = new Dictionary<string, HealthReportEntry>(StringComparer.OrdinalIgnoreCase);

            var totalWatch = Stopwatch.StartNew();

            foreach (var registration in registrations)
            {
                var registrationWatch = Stopwatch.StartNew();
                context.Registration = registration;

                HealthReportEntry entry;
                try
                {
                    var result = await registration.HealthCheckInstance.CheckHealthAsync(context);
                    var duration = registrationWatch.Elapsed;

                    entry = new HealthReportEntry()
                    {
                        Status = result.Status,
                        Description = result.Description,
                        Duration = duration,
                        Exception = result.Exception
                    };
                }
                catch (Exception ex) when (ex as OperationCanceledException == null)
                {
                    var duration = registrationWatch.Elapsed;
                    entry = new HealthReportEntry()
                    {
                        Status = HealthStatus.Unhealthy,
                        Description = ex.Message,
                        Duration = duration,
                        Exception = ex
                    };
                }

                entries[registration.Name] = entry;
            }

            var totalElapsedTime = totalWatch.Elapsed;
            var report = new HealthReport(entries, totalElapsedTime);
            return report;
        }

        private void ValidateRegistrations(IEnumerable<HealthCheckRegistration> registrations)
        {
            if (registrations == null)
             throw new ArgumentNullException(nameof(registrations));

            // Scan the list for duplicate names to provide a better error if there are duplicates.
            var duplicateNames = registrations
                .GroupBy(c => c.Name, StringComparer.OrdinalIgnoreCase)
                .Where(g => g.Count() > 1)
                .Select(g => g.Key)
                .ToList();

            if (duplicateNames.Count > 0)
            {
                throw new ArgumentException($"Duplicate health checks were registered with the name(s): {string.Join(", ", duplicateNames)}", nameof(registrations));
            }
        }
    }
}