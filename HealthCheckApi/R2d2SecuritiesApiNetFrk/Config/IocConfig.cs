﻿using Autofac;
using Autofac.Integration.WebApi;
using R2d2SecuritiesApiNetFrk.Models.HealthCheck.Registration;
using R2d2SecuritiesApiNetFrk.Services;
using System.Reflection;
using System.Web.Http;

namespace R2d2SecuritiesApiNetFrk.Config
{
    public class IocConfig
    {
        public static void Register(HttpConfiguration config)
        {
            var builder = new ContainerBuilder();

            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());
            builder.RegisterType<HealthChecksBuilder>().As<IHealthChecksBuilder>();
            builder.RegisterType<HealthCheckService>().As<IHealthCheckService>();

            var container = builder.Build();

            config.DependencyResolver = new Autofac.Integration.WebApi.AutofacWebApiDependencyResolver(container);
        }
    }
}