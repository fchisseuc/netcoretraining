﻿using Microsoft.Extensions.Diagnostics.HealthChecks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace R2d2SecuritiesApi.HealthCheck
{
    public class GLEntriesServiceHealthCheck : IHealthCheck
    {
        const string GLENTRIES_HEALTH_CHECK_URI = "https://localhost:6001/healthCheck"; //to put in config

        public async Task<HealthCheckResult> CheckHealthAsync(
            HealthCheckContext context,
            CancellationToken cancellationToken = new CancellationToken())
        {
            //TODO: Implement your own healthcheck logic here ex : call an api that the service needs data
            HttpResponseMessage response = null;
            try
            {
                response = await GetGLEntriesHealthCheck();

                bool isHealthy = response.StatusCode == System.Net.HttpStatusCode.OK;

                if (isHealthy)
                    return HealthCheckResult.Healthy($"Succeeded to get GL entries data from {GLENTRIES_HEALTH_CHECK_URI} (status code {(int)response.StatusCode})");
                else
                    return HealthCheckResult.Unhealthy($"Failed to get GL entries data from {GLENTRIES_HEALTH_CHECK_URI} (status code {(int)response.StatusCode})");
            }
            catch (Exception e)
            {
                return HealthCheckResult.Unhealthy($"Failed to get GL entries data from {GLENTRIES_HEALTH_CHECK_URI} (status code {(int)response.StatusCode})", e);
            }
        }

        private async Task<HttpResponseMessage> GetGLEntriesHealthCheck()
        {
            using (var httpClient = new HttpClient())
            {
                var requestMessage = new HttpRequestMessage(HttpMethod.Get, GLENTRIES_HEALTH_CHECK_URI);
                return await httpClient.SendAsync(requestMessage);
            }
        }
    }
}
