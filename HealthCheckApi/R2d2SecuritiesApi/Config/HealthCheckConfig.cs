﻿using HealthChecks.UI.Client;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics.HealthChecks;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Diagnostics.HealthChecks;
using R2d2SecuritiesApi.HealthCheck;

namespace R2d2SecuritiesApi.Config
{
    public static class HealthCheckConfig
    {
        private const int MinimumDriveFreeMB = 100; //100 MB

        public static void AddHealthCheckService(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddHealthChecks()

                //custom health check
                .AddCheck<GLEntriesServiceHealthCheck>("GL entries health check")

                //SQL Server health check
                .AddSqlServer(configuration["ConnectionStrings:DefaultConnection"])

                //Health Check for hosting machine’s disk storage
                .AddDiskStorageHealthCheck(d =>
                    {
                        d.AddDrive(@"C:\", MinimumDriveFreeMB);
                    },
                    "Server Drive",
                    HealthStatus.Degraded
                );
        }

        public static void UseHealthCheckMiddleware(this IApplicationBuilder app)
        {
            //HealthCheck middleware
            app.UseHealthChecks("/healthcheck", new HealthCheckOptions()
            {
                Predicate = healthCheckRegistration => true,
                ResponseWriter = UIResponseWriter.WriteHealthCheckUIResponse
            });
        }
    }
}
