﻿using HealthChecks.UI.Client;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics.HealthChecks;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Diagnostics.HealthChecks;

namespace GLEntriesApi.Config
{
    public static class HealthCheckConfig
    {
        public static void AddHealthCheckService(this IServiceCollection services, IConfiguration Configuration)
        {
            services.AddHealthChecks();
        }

        public static void UseHealthCheckMiddleware(this IApplicationBuilder app)
        {
            //HealthCheck middleware
            app.UseHealthChecks("/healthcheck", new HealthCheckOptions()
            {
                Predicate = healthCheckRegistration => true,
                ResponseWriter = UIResponseWriter.WriteHealthCheckUIResponse
            });
        }
    }
}
