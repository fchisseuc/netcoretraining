﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Hosting.WindowsServices;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace GLEntriesApi
{
    public class Program
    {
        public static void Main(string[] args)
        {
            /*
             * Config to run the app as a service or console
             * https://docs.microsoft.com/fr-fr/aspnet/core/host-and-deploy/windows-service?view=aspnetcore-2.2&tabs=visual-studio
             * https://github.com/dotnet/AspNetCore.Docs/blob/master/aspnetcore/host-and-deploy/windows-service/samples/2.x/AspNetCoreService/Program.cs
             * run as console from publish folder with command : GLEntriesApi.exe --console
             * run as console from project folder with command : dotnet run --console
             */
            var isService = !(Debugger.IsAttached || args.Contains("--console"));

            if (isService)
            {
                var pathToExe = Process.GetCurrentProcess().MainModule.FileName;
                var pathToContentRoot = Path.GetDirectoryName(pathToExe);
                Directory.SetCurrentDirectory(pathToContentRoot);
            }

            var builder = CreateWebHostBuilder(
                args.Where(arg => arg != "--console").ToArray());

            var host = builder.Build();

            if (isService)
            {
                // To run the app without the CustomWebHostService change the
                // next line to host.RunAsService();
                host.RunAsService();
            }
            else
            {
                host.Run();
            }
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .ConfigureLogging((hostingContext, logging) =>
                {
                    logging.AddEventLog();
                })
                .ConfigureAppConfiguration((context, config) =>
                {
                    // Configure the app here.
                })
                //.UseConfiguration(GetLaunchConfiguration())
                .UseStartup<Startup>();

        public static IConfiguration GetLaunchConfiguration() =>
            new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            //.SetBasePath(ContentRootPath)
            .AddJsonFile("hosting.json")
            .Build();
    }
}
