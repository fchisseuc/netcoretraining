$currentPath = (Get-Location);
$parentPath = Split-Path -Parent $currentPath
$params = @{
  Name = "R2D2GLEntriesApi"
  BinaryPathName = "$parentPath\GLEntriesApi.exe"
  StartupType = "Automatic"
  Description = "GL Entries Api hosted as a windows service"
}
New-Service @params
Start-Service -Name "R2D2GLEntriesApi"