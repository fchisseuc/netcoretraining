﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace GLEntriesApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GLEntriesController : ControllerBase
    {
        // GET: api/GLEntries
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }
    }
}
